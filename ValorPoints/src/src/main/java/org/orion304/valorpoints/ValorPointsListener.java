package src.main.java.org.orion304.valorpoints;

import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class ValorPointsListener implements Listener {

	private final ValorPoints plugin;
	private final ConcurrentHashMap<UUID, ValorPointTracker> players = new ConcurrentHashMap<>();

	public ValorPointsListener(ValorPoints instance) {
		this.plugin = instance;
	}

	void clear(boolean giveDailyVp) {
		for (UUID uuid : this.players.keySet()) {
			ValorPointTracker tracker = this.players.get(uuid);
			this.plugin.giveValorPointsForTime(uuid, tracker.getLoggedOnTime());
			if (giveDailyVp) {
				this.plugin.addDailyValorPoints(uuid);
			}
		}
		this.players.clear();
	}

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event) {
		UUID uuid = event.getPlayer().getUniqueId();
		ValorPointTracker tracker;
		if (this.players.containsKey(uuid)) {
			tracker = this.players.get(uuid);
		} else {
			tracker = new ValorPointTracker();
		}
		tracker.trackPlayerJoin();

		this.players.put(uuid, tracker);
	}

	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent event) {
		UUID uuid = event.getPlayer().getUniqueId();
		if (this.players.containsKey(uuid)) {
			ValorPointTracker tracker = this.players.get(uuid);
			tracker.trackPlayerQuit();

		}
	}

	void reload(boolean giveDailyVp) {
		clear(giveDailyVp);
		for (Player player : Bukkit.getOnlinePlayers()) {
			UUID uuid = player.getUniqueId();
			ValorPointTracker tracker = new ValorPointTracker();
			tracker.trackPlayerJoin();
			this.players.put(uuid, tracker);
		}
	}

}
