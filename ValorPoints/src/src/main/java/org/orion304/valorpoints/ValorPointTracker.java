package src.main.java.org.orion304.valorpoints;

public class ValorPointTracker {

	private long starttime;
	private long duration;

	public ValorPointTracker() {
	}

	public long getLoggedOnTime() {
		if (this.starttime == 0) {
			return this.duration;
		}
		return System.currentTimeMillis() - this.starttime;
	}

	public void trackPlayerJoin() {
		this.starttime = System.currentTimeMillis();
	}

	public void trackPlayerQuit() {
		if (this.starttime == 0) {
			return;
		}
		this.duration += System.currentTimeMillis() - this.starttime;
		this.starttime = 0;
	}

}
