package src.main.java.org.orion304.valorpoints;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import src.main.java.org.orion304.OrionPlugin;
import src.main.java.org.orion304.SQLHandler;
import src.main.java.org.orion304.player.CustomPlayer;
import src.main.java.org.orion304.player.CustomPlayerHandler;
import src.main.java.org.orion304.utils.ServerUtils;

public class ValorPoints extends OrionPlugin {

	private final static String host = "127.0.0.1";
	private final static int port = 3306;
	private final static String database = "valorpoints";

	private final static String username = "root";
	private final static String password = "fagba11z";
	private SQLHandler handler;
	private ValorPointsListener listener;
	private ValorPointsThread thread;

	private final int dailyValorAllotment = 25;
	final long hour = 60 * 60 * 1000L;
	long scheduleTime = 0;

	private final double VPperHour = 10;

	private final double VPperMs = this.VPperHour / this.hour;

	protected void addDailyValorPoints(UUID uuid) {
		try {
			PreparedStatement statement = this.handler.getStatement("SELECT * FROM VP WHERE uuid = ?;");
			statement.setString(1, UUIDtoString(uuid));
			ResultSet result = this.handler.get(statement);
			if (!result.first()) {
				addNewPlayerToDatabase(uuid);
			}

			statement = this.handler.getStatement("SELECT * FROM VP WHERE dailyVp < CURDATE() AND uuid=?;");
			statement.setString(1, UUIDtoString(uuid));
			result = this.handler.get(statement);
			if (result.first()) {

				statement = this.handler
						.getStatement("UPDATE VP SET vp = vp + ?, dailyVp = CURRENT_TIMESTAMP WHERE uuid = ?;");
				statement.setInt(1, this.dailyValorAllotment);
				statement.setString(2, UUIDtoString(uuid));
				this.handler.update(statement);
				Player player = Bukkit.getPlayer(uuid);
				if (player != null) {
					ServerUtils.sendMessage(player, ChatColor.GOLD + "You have earned " + this.dailyValorAllotment
							+ "VP" + ChatColor.GOLD.toString() + " for logging in today.");
				}
				return;

			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private void addNewPlayerToDatabase(UUID uuid) throws SQLException {
		PreparedStatement statement = this.handler.getStatement("INSERT INTO VP SET uuid=?, dailyVp=0;");
		statement.setString(1, UUIDtoString(uuid));
		this.handler.update(statement);
	}

	public void addValorPoints(UUID uuid, int valorPoints) {
		try {
			PreparedStatement statement = this.handler.getStatement("SELECT * FROM VP WHERE uuid = ?;");
			statement.setString(1, UUIDtoString(uuid));
			ResultSet result = this.handler.get(statement);
			if (!result.first()) {
				addNewPlayerToDatabase(uuid);
			}
			statement = this.handler.getStatement("UPDATE VP SET vp = vp + ?, username = ? WHERE uuid = ?;");
			statement.setInt(1, valorPoints);
			statement.setString(2, Bukkit.getOfflinePlayer(uuid).getName());
			statement.setString(3, UUIDtoString(uuid));
			Player player = Bukkit.getPlayer(uuid);
			if (player != null) {
				if (valorPoints < 0) {
					ServerUtils.sendMessage(player, ChatColor.GOLD.toString() + -valorPoints + "VP"
							+ " have been deducted from your account for your purchase.");
				} else {
					ServerUtils.sendMessage(player,
							ChatColor.GOLD + "You have earned " + valorPoints + "VP" + " for your activity.");
				}
			}
			this.handler.update(statement);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void disable() {

		this.listener.clear(false);
	}

	protected void distributeAndReload() {
		this.listener.reload(true);
	}

	public boolean doesUserExist(UUID uuid) {
		try {
			PreparedStatement statement = this.handler.getStatement("SELECT * FROM VP WHERE uuid = ?;");
			statement.setString(1, UUIDtoString(uuid));
			ResultSet result = this.handler.get(statement);
			return result.first();
		} catch (SQLException e) {
			return false;
		}
	}

	@Override
	public void enable() {
		this.thread = new ValorPointsThread(this);

		this.handler = new SQLHandler(this, host, port, database, username, password);
		ensureFormat();

		this.listener = new ValorPointsListener(this);
		this.listener.reload(false);
		this.manager.registerEvents(this.listener, this);

		Calendar calendar = new GregorianCalendar();
		calendar.setTimeInMillis(System.currentTimeMillis());
		int minutes = calendar.get(Calendar.MINUTE);
		int seconds = calendar.get(Calendar.SECOND);
		long delay = this.hour - 1000L * (minutes * 60 + seconds);
		this.scheduleTime = System.currentTimeMillis() + delay;
		delay *= (20. / 1000.);
		long timer = (long) (this.hour * (20. / 1000.));

		this.scheduler.runTaskTimerAsynchronously(this, this.thread, delay, timer);

		this.scheduler.runTaskTimerAsynchronously(this, new KeepAliveThread(this.handler), 20 * 60, 20 * 60);

	}

	private void ensureFormat() {
		try {
			PreparedStatement statement = this.handler.getStatement("SHOW TABLES LIKE 'VP';");
			ResultSet set = this.handler.get(statement);
			if (!set.first()) {
				statement = this.handler.getStatement(
						"CREATE TABLE VP(uuid VARCHAR(32) primary key, username VARCHAR(16), vp INT default 0, dailyVp TIMESTAMP default 0);");
				this.handler.update(statement);
			}
			this.handler.update("ALTER TABLE VP ALTER vp SET DEFAULT 0;");
			this.handler.update("ALTER TABLE VP ALTER dailyVp SET DEFAULT 0;");

			statement = this.handler.getStatement("SHOW TABLES LIKE 'accessLogs';");
			set = this.handler.get(statement);
			if (!set.first()) {
				statement = this.handler.getStatement(
						"CREATE TABLE accessLogs(pointer TINYINT(1) default 1, server VARCHAR(32), date TIMESTAMP default 0);");
				this.handler.update(statement);
				this.handler.update("INSERT INTO accessLogs SET pointer = 1, server = 'none', date = 0;");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public CustomPlayerHandler<? extends OrionPlugin, ? extends CustomPlayer<? extends OrionPlugin>> getCustomPlayerHandler() {
		// TODO Auto-generated method stub
		return null;
	}

	public int getValorPoints(UUID uuid) {
		try {
			PreparedStatement statement = this.handler.getStatement("SELECT * FROM VP WHERE uuid = ?;");
			statement.setString(1, UUIDtoString(uuid));
			ResultSet result = this.handler.get(statement);
			if (result.first()) {
				return result.getInt("vp");
			} else {
				statement = this.handler.getStatement("INSERT INTO VP SET uuid=?;");
				statement.setString(1, UUIDtoString(uuid));
				this.handler.update(statement);
				return 0;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}

	protected void giveValorPointsForTime(UUID uuid, long duration) {
		double points = Math.rint(duration * this.VPperMs);
		addValorPoints(uuid, (int) points);
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		new ValorPointsCommandHandler(this, sender, cmd, args);
		return true;
	}

	protected void recordAccess() {
		try {
			PreparedStatement statement = this.handler
					.getStatement("UPDATE accessLogs SET date = CURRENT_TIMESTAMP, server = ? WHERE pointer = 1;");
			statement.setString(1, Bukkit.getServerName());
			this.handler.update(statement);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private String UUIDtoString(UUID uuid) {
		return uuid.toString().replaceAll("-", "");

	}

	protected boolean wasDatabaseRecentlyAccessed() {
		try {
			ResultSet set = this.handler.get(
					"SELECT * FROM accessLogs WHERE pointer=1 AND date < (CURRENT_TIMESTAMP - INTERVAL 2 SECOND);");
			return !set.first();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
}
