package src.main.java.org.orion304.valorpoints;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import src.main.java.org.orion304.utils.ServerUtils;

public class ValorPointsCommandHandler {

	private final ValorPoints plugin;
	private final Player player;
	private final String[] args;

	public ValorPointsCommandHandler(ValorPoints plugin, CommandSender sender, Command cmd, String[] args) {
		this.plugin = plugin;
		this.args = args;
		if (sender instanceof Player) {
			this.player = (Player) sender;
		} else {
			this.player = null;
		}

		switch (cmd.getName()) {
		case "showvp":
			showVp();
			break;
		case "addvp":
			addVp();
			break;
		case "vpcommand":
			vpCommand();
			break;
		case "vpwhen":
			vpWhen();
			break;
		}
	}

	private void addVp() {
		if (!ServerUtils.hasPermission(this.player, "valorpoints.add")) {
			ServerUtils.sendMessage(this.player, "You do not have permission to do that.");
			return;
		}
		if (this.args.length != 2) {
			ServerUtils.sendMessage(this.player, "Usage: /addvp <player> <vp>");
			return;
		}
		String playerName = this.args[0];
		Player player = Bukkit.getPlayer(playerName);
		if (player == null) {
			ServerUtils.sendMessage(this.player, "That player is not online.");
			return;
		}
		int points = 0;
		try {
			points = Integer.parseInt(this.args[1]);
		} catch (NumberFormatException e) {

		}

		this.plugin.addValorPoints(player.getUniqueId(), points);
		ServerUtils.sendMessage(this.player, "Added " + points + " VP to " + playerName + "'s balance.");
	}

	private void showVp() {
		if (!ServerUtils.hasPermission(this.player, "valorpoints.show")) {
			return;
		}
		String color = ChatColor.GOLD.toString() + ChatColor.BOLD.toString();
		if (this.args.length == 0) {
			if (this.player == null) {
				ServerUtils.sendMessage(this.player, "Command line usage: /showvp <player>");
			} else {
				ServerUtils.sendMessage(this.player,
						"Your Valor Point balance: " + color + this.plugin.getValorPoints(this.player.getUniqueId()));
			}
		} else {
			if (ServerUtils.hasPermission(this.player, "valorpoints.showother")) {
				Player player = Bukkit.getPlayer(this.args[0]);
				if (player != null && this.plugin.doesUserExist(player.getUniqueId())) {
					ServerUtils.sendMessage(this.player, this.args[0] + "'s Valor Point balance: " + color
							+ this.plugin.getValorPoints(player.getUniqueId()));
				} else {
					ServerUtils.sendMessage(this.player, this.args[0] + " is not online.");
				}
			} else {
				ServerUtils.sendMessage(this.player, "Usage: /showvp");
			}
		}
	}

	private void vpCommand() {
		if (!ServerUtils.hasPermission(this.player, "valorpoints.command")) {
			ServerUtils.sendMessage(this.player, "You do not have permission to use this command.");
			return;
		}
		String usageMessage = "Usage: /vpcommand <player> <vp> [(-)permission:node] <command>";
		if (this.args.length < 3) {
			ServerUtils.sendMessage(this.player, usageMessage);
			return;
		}

		String target = this.args[0];
		Player targetPlayer = Bukkit.getPlayer(target);
		if (targetPlayer == null) {
			ServerUtils.sendMessage(this.player, "Player is not online.");
			return;
		}
		int vpCost = 0;
		try {
			vpCost = Integer.parseInt(this.args[1]);
		} catch (NumberFormatException e) {
			ServerUtils.sendMessage(this.player, usageMessage);
			return;
		}

		int vp = this.plugin.getValorPoints(targetPlayer.getUniqueId());

		String command = "";
		CommandSender sender = Bukkit.getConsoleSender();

		int starti = 2;

		String permission = this.args[2];
		if (permission.startsWith("permission:") || permission.startsWith("-permission:")) {
			boolean checkForHavingPermission = !permission.startsWith("-");
			permission = permission.split(":", 2)[1];
			if (checkForHavingPermission != targetPlayer.hasPermission(permission)) {
				return;
			} else {
				starti = 3;
				if (this.args.length < 4) {
					ServerUtils.sendMessage(this.player, usageMessage);
					return;
				}
			}
		}

		for (int i = starti; i < this.args.length; i++) {
			command += this.args[i];
			if (i < this.args.length) {
				command += " ";
			}
		}

		if (vp >= vpCost) {
			boolean success = Bukkit.dispatchCommand(sender, command);
			if (success) {
				this.plugin.addValorPoints(targetPlayer.getUniqueId(), -vpCost);
			} else {
				ServerUtils.sendMessage(this.player, "This command doesn't exist or didn't work.");
			}
		} else {
			ServerUtils.sendMessage(this.player, target + " does not have the required VP.");
			ServerUtils.sendMessage(targetPlayer, "You do not have the necessary VP for this purchase.");
		}

	}

	private void vpWhen() {
		long time = this.plugin.scheduleTime - System.currentTimeMillis();
		long minutes = time / (60 * 1000);
		ServerUtils.sendMessage(this.player, "Valor Points will be allotted in " + minutes + " minutes.");
	}
}
