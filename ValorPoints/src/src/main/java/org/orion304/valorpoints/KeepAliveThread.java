package src.main.java.org.orion304.valorpoints;

import src.main.java.org.orion304.SQLHandler;

public class KeepAliveThread implements Runnable {

    private final SQLHandler handler;

    public KeepAliveThread(final SQLHandler handler) {
	this.handler = handler;
    }

    @Override
    public void run() {
	this.handler.connect();
    }

}
