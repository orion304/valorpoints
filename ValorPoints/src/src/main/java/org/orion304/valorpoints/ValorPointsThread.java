package src.main.java.org.orion304.valorpoints;

import java.util.Random;

public class ValorPointsThread implements Runnable {

	private final ValorPoints plugin;
	private final Random random = new Random();

	public ValorPointsThread(ValorPoints instance) {
		this.plugin = instance;
	}

	@Override
	public void run() {
		this.plugin.scheduleTime += this.plugin.hour;
		while (this.plugin.wasDatabaseRecentlyAccessed()) {
			try {
				Thread.sleep(2000L + this.random.nextInt(100) * 10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		this.plugin.recordAccess();
		this.plugin.distributeAndReload();
	}

}
